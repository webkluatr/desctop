# kluatr-prototip-1
Прототип проектов клуатра (сайт)

[Документация](docs-md/ApiIndex.md)

в качестве сборщика используется ant
~~~~
apt install ant
~~~~

для него может потребоваться пакет oopenjdk-8-jdk
~~~~ 
apt install openjdk-8-jdk
~~~~
#Develop build
~~~~ 
ant -f ./build-dev.xml
~~~~
это билд для разработки, будут обновлены зависимости,
выполнены юнит тесты, обновлена документация

#Production build 
~~~~ 
ant 
~~~~
будет выполнено только обновление зависимостей

Для обратной совместимости с текущим проектом kluatr
в `composer.json` добавлено
~~~~
 "autoload": {
  ....
  "classmap":[
            "../../kluatr/classes",
            "../../kluatr/system"
        ],
         "files":[   "../../kluatr/bootstrap.php"
        ]
~~~~
это путь к старому проекту относительно корня текущего