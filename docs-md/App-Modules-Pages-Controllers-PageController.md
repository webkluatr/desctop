App\Modules\Pages\Controllers\PageController
===============

PageController маршруты для статических страниц.

PageController выполняет маршрутизацию для отрисовки ститических страниц.


* Class name: PageController
* Namespace: App\Modules\Pages\Controllers
* Parent class: [Kluatr\Core\Components\Web\AWebController](Kluatr-Core-Components-Web-AWebController.md)





Properties
----------


### $_pageTitle

    protected mixed $_pageTitle





* Visibility: **protected**


### $_caption

    protected mixed $_caption





* Visibility: **protected**


### $breadcrumbs

    public mixed $breadcrumbs = array()





* Visibility: **public**


### $layout

    public mixed $layout = "@currentTheme/views/layouts/main"





* Visibility: **public**


Methods
-------


### actionMain

    string App\Modules\Pages\Controllers\PageController::actionMain()

Описывает роут для главной страницы



* Visibility: **public**




### actionError

    string App\Modules\Pages\Controllers\PageController::actionError()

Дефолтный метод обрабатывающий исключения

@todo можно добавить сохранние исключений в бд

* Visibility: **public**




### init

    mixed Kluatr\Core\Components\Web\IController::init()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### setCaption

    mixed Kluatr\Core\Components\Web\IController::setCaption(\Kluatr\Core\Components\Web\string $caption)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**



### getCaption

    mixed Kluatr\Core\Components\Web\IController::getCaption()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### setPageTitle

    mixed Kluatr\Core\Components\Web\IController::setPageTitle(\Kluatr\Core\Components\Web\string $value)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $value **Kluatr\Core\Components\Web\string**



### getPageTitle

    mixed Kluatr\Core\Components\Web\IController::getPageTitle()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### addBreadcrumb

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumb(\Kluatr\Core\Components\Web\string $caption, \Kluatr\Core\Components\Web\string $link)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**
* $link **Kluatr\Core\Components\Web\string**



### addBreadcrumbs

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumbs(array $breadcrumbs)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $breadcrumbs **array**



### render

    mixed Kluatr\Core\Components\Web\IController::render($view, $params)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $view **mixed**
* $params **mixed**


