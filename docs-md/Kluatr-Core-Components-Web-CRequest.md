Kluatr\Core\Components\Web\CRequest
===============






* Class name: CRequest
* Namespace: Kluatr\Core\Components\Web
* Parent class: yii\web\Request





Properties
----------


### $_cookies

    private mixed $_cookies





* Visibility: **private**


Methods
-------


### getCookies

    mixed Kluatr\Core\Components\Web\CRequest::getCookies()





* Visibility: **public**



