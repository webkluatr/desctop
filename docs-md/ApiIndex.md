API Index
=========

* Kluatr
    * Kluatr\Core
        * Kluatr\Core\Components
            * Kluatr\Core\Components\Web
                * Kluatr\Core\Components\Web\Managers
                    * [CUrlManager](Kluatr-Core-Components-Web-Managers-CUrlManager.md)
                    * [CUrlRule](Kluatr-Core-Components-Web-Managers-CUrlRule.md)
                * [AApplication](Kluatr-Core-Components-Web-AApplication.md)
                * [IController](Kluatr-Core-Components-Web-IController.md)
                * [AWebModule](Kluatr-Core-Components-Web-AWebModule.md)
                * [CRequest](Kluatr-Core-Components-Web-CRequest.md)
                * [CResponse](Kluatr-Core-Components-Web-CResponse.md)
                * [CCookieCollection](Kluatr-Core-Components-Web-CCookieCollection.md)
                * [AWebController](Kluatr-Core-Components-Web-AWebController.md)
            * Kluatr\Core\Components\Db
                * [ICommand](Kluatr-Core-Components-Db-ICommand.md)
                * [CCommand](Kluatr-Core-Components-Db-CCommand.md)
            * Kluatr\Core\Components\User
                * [WebUser](Kluatr-Core-Components-User-WebUser.md)
                * [CIdentity](Kluatr-Core-Components-User-CIdentity.md)
            * Kluatr\Core\Components\Errors
                * [BaseException](Kluatr-Core-Components-Errors-BaseException.md)
* App
    * App\Modules
        * App\Modules\Pages
            * [PagesModule](App-Modules-Pages-PagesModule.md)
        * App\Modules\Legasy
            * [LegasyModule](App-Modules-Legasy-LegasyModule.md)
            * App\Modules\Legasy\Controllers
                * [ProxyController](App-Modules-Legasy-Controllers-ProxyController.md)
    * App\Config
        * [IConfigEnv](App-Config-IConfigEnv.md)

