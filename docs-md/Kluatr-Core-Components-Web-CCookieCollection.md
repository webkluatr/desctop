Kluatr\Core\Components\Web\CCookieCollection
===============






* Class name: CCookieCollection
* Namespace: Kluatr\Core\Components\Web
* Parent class: yii\web\CookieCollection





Properties
----------


### $_cookies

    private mixed $_cookies = array()





* Visibility: **private**


Methods
-------


### add

    mixed Kluatr\Core\Components\Web\CCookieCollection::add(\Kluatr\Core\Components\Web\Cookie $cookie)

Adds a cookie to the collection.

If there is already a cookie with the same name in the collection, it will be removed first.

* Visibility: **public**


#### Arguments
* $cookie **Kluatr\Core\Components\Web\Cookie** - &lt;p&gt;the cookie to be added&lt;/p&gt;


