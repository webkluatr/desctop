Kluatr\Core\Components\Web\CResponse
===============






* Class name: CResponse
* Namespace: Kluatr\Core\Components\Web
* Parent class: yii\web\Response





Properties
----------


### $_cookies

    private mixed $_cookies





* Visibility: **private**


Methods
-------


### send

    mixed Kluatr\Core\Components\Web\CResponse::send()





* Visibility: **public**




### sendCookies

    mixed Kluatr\Core\Components\Web\CResponse::sendCookies()

Sends the cookies to the client.



* Visibility: **protected**




### addCookieItem

    mixed Kluatr\Core\Components\Web\CResponse::addCookieItem($cookie)





* Visibility: **public**


#### Arguments
* $cookie **mixed**



### removeCookieItem

    mixed Kluatr\Core\Components\Web\CResponse::removeCookieItem($cookieName)





* Visibility: **public**


#### Arguments
* $cookieName **mixed**



### setCookies

    mixed Kluatr\Core\Components\Web\CResponse::setCookies()





* Visibility: **public**



