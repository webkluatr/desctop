Kluatr\Core\Components\Web\Managers\CUrlRule
===============






* Class name: CUrlRule
* Namespace: Kluatr\Core\Components\Web\Managers
* Parent class: yii\web\UrlRule





Properties
----------


### $_routeParams

    private mixed $_routeParams





* Visibility: **private**


### $_paramRules

    private mixed $_paramRules





* Visibility: **private**


### $_routeRule

    private mixed $_routeRule





* Visibility: **private**


### $_template

    private mixed $_template





* Visibility: **private**


Methods
-------


### init

    mixed Kluatr\Core\Components\Web\Managers\CUrlRule::init()

Initializes this rule.



* Visibility: **public**




### parseRequest

    array|boolean Kluatr\Core\Components\Web\Managers\CUrlRule::parseRequest(\Kluatr\Core\Components\Web\Managers\UrlManager $manager, \Kluatr\Core\Components\Web\Managers\Request $request)

Parses the given request and returns the corresponding route and parameters.



* Visibility: **public**


#### Arguments
* $manager **Kluatr\Core\Components\Web\Managers\UrlManager** - &lt;p&gt;the URL manager&lt;/p&gt;
* $request **Kluatr\Core\Components\Web\Managers\Request** - &lt;p&gt;the request component&lt;/p&gt;


