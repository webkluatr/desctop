Kluatr\Core\Components\User\WebUser
===============

Class WebUser
Основной компонент работы с пользователем




* Class name: WebUser
* Namespace: Kluatr\Core\Components\User
* Parent class: yii\web\User
* This class implements: Kluatr\Core\Components\User\IBaseUser




Properties
----------


### $_user

    private mixed $_user





* Visibility: **private**


Methods
-------


### init

    mixed Kluatr\Core\Components\User\WebUser::init()





* Visibility: **public**




### _get_access_keys

    mixed Kluatr\Core\Components\User\WebUser::_get_access_keys()





* Visibility: **private**




### can

    mixed Kluatr\Core\Components\User\WebUser::can($permissionName, $params, $allowCaching)





* Visibility: **public**


#### Arguments
* $permissionName **mixed**
* $params **mixed**
* $allowCaching **mixed**



### getUser

    mixed Kluatr\Core\Components\User\WebUser::getUser()





* Visibility: **public**




### login

    mixed Kluatr\Core\Components\User\WebUser::login(\yii\web\IdentityInterface $identity, $duration)





* Visibility: **public**


#### Arguments
* $identity **yii\web\IdentityInterface**
* $duration **mixed**



### logout

    mixed Kluatr\Core\Components\User\WebUser::logout($destroySession)





* Visibility: **public**


#### Arguments
* $destroySession **mixed**


