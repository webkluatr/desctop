Kluatr\Core\Components\Db\ICommand
===============






* Interface name: ICommand
* Namespace: Kluatr\Core\Components\Db
* This is an **interface**






Methods
-------


### queryOne

    mixed Kluatr\Core\Components\Db\ICommand::queryOne($fetchMode)





* Visibility: **public**


#### Arguments
* $fetchMode **mixed**



### queryAll

    mixed Kluatr\Core\Components\Db\ICommand::queryAll($fetchMode)





* Visibility: **public**


#### Arguments
* $fetchMode **mixed**



### setFetchMode

    mixed Kluatr\Core\Components\Db\ICommand::setFetchMode($mode, $class)





* Visibility: **public**


#### Arguments
* $mode **mixed**
* $class **mixed**



### bindParam

    mixed Kluatr\Core\Components\Db\ICommand::bindParam($name, $value, $dataType, $length, $driverOptions)





* Visibility: **public**


#### Arguments
* $name **mixed**
* $value **mixed**
* $dataType **mixed**
* $length **mixed**
* $driverOptions **mixed**



### bindValue

    mixed Kluatr\Core\Components\Db\ICommand::bindValue($name, $value, $dataType)





* Visibility: **public**


#### Arguments
* $name **mixed**
* $value **mixed**
* $dataType **mixed**



### bindValues

    mixed Kluatr\Core\Components\Db\ICommand::bindValues($values)





* Visibility: **public**


#### Arguments
* $values **mixed**



### query

    mixed Kluatr\Core\Components\Db\ICommand::query()





* Visibility: **public**



