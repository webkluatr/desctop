App\Modules\Legasy\LegasyModule
===============






* Class name: LegasyModule
* Namespace: App\Modules\Legasy
* Parent class: [Kluatr\Core\Components\Web\AWebModule](Kluatr-Core-Components-Web-AWebModule.md)





Properties
----------


### $controllerNamespace

    public mixed $controllerNamespace = "App\Modules\Legasy\Controllers"





* Visibility: **public**


### $urlRules

    public mixed $urlRules = array()





* Visibility: **public**


Methods
-------


### init

    mixed Kluatr\Core\Components\Web\AWebModule::init()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\AWebModule](Kluatr-Core-Components-Web-AWebModule.md)




### _add_url_rule

    mixed Kluatr\Core\Components\Web\AWebModule::_add_url_rule($key, $path, $verb)





* Visibility: **protected**
* This method is defined by [Kluatr\Core\Components\Web\AWebModule](Kluatr-Core-Components-Web-AWebModule.md)


#### Arguments
* $key **mixed**
* $path **mixed**
* $verb **mixed**


