Kluatr\Core\Components\Web\AWebModule
===============






* Class name: AWebModule
* Namespace: Kluatr\Core\Components\Web
* This is an **abstract** class
* Parent class: yii\base\Module





Properties
----------


### $urlRules

    public mixed $urlRules = array()





* Visibility: **public**


Methods
-------


### init

    mixed Kluatr\Core\Components\Web\AWebModule::init()





* Visibility: **public**




### _add_url_rule

    mixed Kluatr\Core\Components\Web\AWebModule::_add_url_rule($key, $path, $verb)





* Visibility: **protected**


#### Arguments
* $key **mixed**
* $path **mixed**
* $verb **mixed**


