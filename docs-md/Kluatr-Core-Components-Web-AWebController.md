Kluatr\Core\Components\Web\AWebController
===============






* Class name: AWebController
* Namespace: Kluatr\Core\Components\Web
* This is an **abstract** class
* Parent class: yii\web\Controller
* This class implements: [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




Properties
----------


### $_pageTitle

    protected mixed $_pageTitle





* Visibility: **protected**


### $_caption

    protected mixed $_caption





* Visibility: **protected**


### $breadcrumbs

    public mixed $breadcrumbs = array()





* Visibility: **public**


### $layout

    public mixed $layout = "@currentTheme/views/layouts/main"





* Visibility: **public**


Methods
-------


### init

    mixed Kluatr\Core\Components\Web\IController::init()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### setCaption

    mixed Kluatr\Core\Components\Web\IController::setCaption(\Kluatr\Core\Components\Web\string $caption)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**



### getCaption

    mixed Kluatr\Core\Components\Web\IController::getCaption()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### setPageTitle

    mixed Kluatr\Core\Components\Web\IController::setPageTitle(\Kluatr\Core\Components\Web\string $value)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $value **Kluatr\Core\Components\Web\string**



### getPageTitle

    mixed Kluatr\Core\Components\Web\IController::getPageTitle()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)




### addBreadcrumb

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumb(\Kluatr\Core\Components\Web\string $caption, \Kluatr\Core\Components\Web\string $link)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**
* $link **Kluatr\Core\Components\Web\string**



### addBreadcrumbs

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumbs(array $breadcrumbs)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $breadcrumbs **array**



### render

    mixed Kluatr\Core\Components\Web\IController::render($view, $params)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Web\IController](Kluatr-Core-Components-Web-IController.md)


#### Arguments
* $view **mixed**
* $params **mixed**


