Kluatr\Core\Components\Errors\BaseException
===============






* Class name: BaseException
* Namespace: Kluatr\Core\Components\Errors
* Parent class: Exception





Properties
----------


### $_info_tpl

    private mixed $_info_tpl = "
                 code: -code-
                 message: -message-
                 file: -file-
                 line: -line-
                 stack: -stack-
                 ====="





* Visibility: **private**


Methods
-------


### __construct

    mixed Kluatr\Core\Components\Errors\BaseException::__construct(\Kluatr\Core\Components\Errors\string $message, \Kluatr\Core\Components\Errors\int $code, \Throwable $previous)





* Visibility: **public**


#### Arguments
* $message **Kluatr\Core\Components\Errors\string**
* $code **Kluatr\Core\Components\Errors\int**
* $previous **Throwable**



### getErrorTpl

    string Kluatr\Core\Components\Errors\BaseException::getErrorTpl()

Возвращает шаблон текстового представления ошибки



* Visibility: **public**




### getErrorStringInfo

    string Kluatr\Core\Components\Errors\BaseException::getErrorStringInfo()

Возвращает текстовую информацию по исключению
и вложенным ошибкам



* Visibility: **public**



