Kluatr\Core\Components\User\CIdentity
===============

Class CIdentity
Основной инструмент идендификации пользователя




* Class name: CIdentity
* Namespace: Kluatr\Core\Components\User
* Parent class: yii\base\Component
* This class implements: yii\web\IdentityInterface, Kluatr\Core\Components\User\IBaseUser




Properties
----------


### $id

    protected mixed $id





* Visibility: **protected**


### $email

    protected mixed $email





* Visibility: **protected**


### $authKey

    public mixed $authKey = '936c1f96-fd7c-4c33-a688-d22e700d072e'





* Visibility: **public**


### $login

    public mixed $login





* Visibility: **public**


### $password

    public mixed $password





* Visibility: **public**


### $_state

    protected mixed $_state = array()





* Visibility: **protected**


### $isValid

    public mixed $isValid = false





* Visibility: **public**


### $LastMessage

    public mixed $LastMessage





* Visibility: **public**


### $user

    protected mixed $user





* Visibility: **protected**


Methods
-------


### __construct

    mixed Kluatr\Core\Components\User\CIdentity::__construct($login, $password)





* Visibility: **public**


#### Arguments
* $login **mixed**
* $password **mixed**



### chekLogin

    mixed Kluatr\Core\Components\User\CIdentity::chekLogin()





* Visibility: **public**




### getUserInfo

    mixed Kluatr\Core\Components\User\CIdentity::getUserInfo()





* Visibility: **public**




### getTokenInfo

    mixed Kluatr\Core\Components\User\CIdentity::getTokenInfo()





* Visibility: **public**




### chekToken

    mixed Kluatr\Core\Components\User\CIdentity::chekToken($token)





* Visibility: **public**


#### Arguments
* $token **mixed**



### getState

    mixed Kluatr\Core\Components\User\CIdentity::getState(string $name, mixed $defaultValue)

Gets the persisted state by the specified name.



* Visibility: **public**


#### Arguments
* $name **string** - &lt;p&gt;the name of the state&lt;/p&gt;
* $defaultValue **mixed** - &lt;p&gt;the default value to be returned if the named state does not exist&lt;/p&gt;



### setState

    mixed Kluatr\Core\Components\User\CIdentity::setState(string $name, mixed $value)

Sets the named state with a given value.



* Visibility: **public**


#### Arguments
* $name **string** - &lt;p&gt;the name of the state&lt;/p&gt;
* $value **mixed** - &lt;p&gt;the value of the named state&lt;/p&gt;



### findIdentity

    mixed Kluatr\Core\Components\User\CIdentity::findIdentity($id)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $id **mixed**



### findIdentityByAccessToken

    mixed Kluatr\Core\Components\User\CIdentity::findIdentityByAccessToken($token, $type)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $token **mixed**
* $type **mixed**



### getId

    mixed Kluatr\Core\Components\User\CIdentity::getId()





* Visibility: **public**




### getAuthKey

    mixed Kluatr\Core\Components\User\CIdentity::getAuthKey()





* Visibility: **public**




### validateAuthKey

    mixed Kluatr\Core\Components\User\CIdentity::validateAuthKey($authKey)





* Visibility: **public**


#### Arguments
* $authKey **mixed**


