Kluatr\Core\Components\Web\Managers\CUrlManager
===============

CUrlManager short summary.

CUrlManager description.


* Class name: CUrlManager
* Namespace: Kluatr\Core\Components\Web\Managers
* Parent class: yii\web\UrlManager







Methods
-------


### parseRequest

    array|boolean Kluatr\Core\Components\Web\Managers\CUrlManager::parseRequest(\Kluatr\Core\Components\Web\Managers\Request $request)

Parses the user request.



* Visibility: **public**


#### Arguments
* $request **Kluatr\Core\Components\Web\Managers\Request** - &lt;p&gt;the request component&lt;/p&gt;


