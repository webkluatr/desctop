Kluatr\Core\Components\Web\IController
===============






* Interface name: IController
* Namespace: Kluatr\Core\Components\Web
* This is an **interface**






Methods
-------


### init

    mixed Kluatr\Core\Components\Web\IController::init()





* Visibility: **public**




### setCaption

    mixed Kluatr\Core\Components\Web\IController::setCaption(\Kluatr\Core\Components\Web\string $caption)





* Visibility: **public**


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**



### getCaption

    mixed Kluatr\Core\Components\Web\IController::getCaption()





* Visibility: **public**




### setPageTitle

    mixed Kluatr\Core\Components\Web\IController::setPageTitle(\Kluatr\Core\Components\Web\string $value)





* Visibility: **public**


#### Arguments
* $value **Kluatr\Core\Components\Web\string**



### getPageTitle

    mixed Kluatr\Core\Components\Web\IController::getPageTitle()





* Visibility: **public**




### addBreadcrumb

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumb(\Kluatr\Core\Components\Web\string $caption, \Kluatr\Core\Components\Web\string $link)





* Visibility: **public**


#### Arguments
* $caption **Kluatr\Core\Components\Web\string**
* $link **Kluatr\Core\Components\Web\string**



### addBreadcrumbs

    mixed Kluatr\Core\Components\Web\IController::addBreadcrumbs(array $breadcrumbs)





* Visibility: **public**


#### Arguments
* $breadcrumbs **array**



### render

    mixed Kluatr\Core\Components\Web\IController::render($view, $params)





* Visibility: **public**


#### Arguments
* $view **mixed**
* $params **mixed**


