Kluatr\Core\Components\Db\CCommand
===============






* Class name: CCommand
* Namespace: Kluatr\Core\Components\Db
* Parent class: yii\db\Command
* This class implements: [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)






Methods
-------


### setFetchMode

    mixed Kluatr\Core\Components\Db\ICommand::setFetchMode($mode, $class)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $mode **mixed**
* $class **mixed**



### queryInternal

    mixed Kluatr\Core\Components\Db\CCommand::queryInternal(string $method, integer $fetchMode)

Performs the actual DB query of a SQL statement.



* Visibility: **protected**


#### Arguments
* $method **string** - &lt;p&gt;method of PDOStatement to be called&lt;/p&gt;
* $fetchMode **integer** - &lt;p&gt;the result fetch mode. Please refer to &lt;a href=&quot;http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php&quot;&gt;PHP manual&lt;/a&gt;
for valid fetch modes. If this parameter is null, the value set in [[fetchMode]] will be used.&lt;/p&gt;



### queryOne

    mixed Kluatr\Core\Components\Db\ICommand::queryOne($fetchMode)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $fetchMode **mixed**



### queryAll

    mixed Kluatr\Core\Components\Db\ICommand::queryAll($fetchMode)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $fetchMode **mixed**



### bindParam

    mixed Kluatr\Core\Components\Db\ICommand::bindParam($name, $value, $dataType, $length, $driverOptions)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $name **mixed**
* $value **mixed**
* $dataType **mixed**
* $length **mixed**
* $driverOptions **mixed**



### bindValue

    mixed Kluatr\Core\Components\Db\ICommand::bindValue($name, $value, $dataType)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $name **mixed**
* $value **mixed**
* $dataType **mixed**



### bindValues

    mixed Kluatr\Core\Components\Db\ICommand::bindValues($values)





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)


#### Arguments
* $values **mixed**



### query

    mixed Kluatr\Core\Components\Db\ICommand::query()





* Visibility: **public**
* This method is defined by [Kluatr\Core\Components\Db\ICommand](Kluatr-Core-Components-Db-ICommand.md)



