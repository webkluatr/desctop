<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 23.01.18
 * Time: 13:38
 */
namespace Tests\Core\Helpers;
use PHPUnit\Framework\TestCase;
use Kluatr\Core\Helpers;

final class GlobalTest extends TestCase
{
    public function testPackagingObjectToString()
    {
        $object = new \stdClass();
        $object->property1 = "string";
        $str = Helpers\packaging_object_to_string($object);
        $this->assertEquals(
            $str
            , "q1YqKMovSC0qqTRUslIqLinKzEtXqgUA"
            , "Packaging Alghoritm was changed!"
        );
    }

    public function testUnPackagingStringToObject()
    {
        $object = new \stdClass();
        $object->property1 = "string";
        $testObject = Helpers\un_packaging_string_to_object("q1YqKMovSC0qqTRUslIqLinKzEtXqgUA");
        $this->assertFalse($testObject != $object
            , "Packaging Alghoritm was changed!"
        );
    }
}
