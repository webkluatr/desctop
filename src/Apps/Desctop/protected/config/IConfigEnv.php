<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 23.01.18
 * Time: 17:02
 */

namespace App\Config;

interface IConfigEnv
{
    /**
     * Среда в которой запускается проект
     */
    const ENVIRONMENT = "develop";
}
