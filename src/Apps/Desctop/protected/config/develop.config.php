<?php

\defined('YII_DEBUG') or \define('YII_DEBUG', false);
\ini_set('date.timezone', 'Europe/Moscow');
\date_default_timezone_set('Europe/Moscow');
$theme_selected = "basic";
$config = [
    'id' => 'Desctop',
    'basePath' => \dirname(__DIR__),
    'bootstrap' => ['Application'],
    'defaultRoute' => 'pages/page/main',
    'controllerNamespace' => "",
    'aliases' => [
        '@currentTheme' => '@app/Themes/'.$theme_selected,
    ],
    'components' => [
        'view' => [
            'theme' => [
                'basePath' => '@currentTheme',
                'baseUrl' => '@web/themes/'.$theme_selected,
                'pathMap' => [],
            ],
        ],
        'Application' => [
            'class' => 'Kluatr\Core\Components\Web\AApplication'
        ],
        'errorHandler' => [
            'errorAction' => 'pages/page/error',
        ],
        'response' => [
            'class' => 'Kluatr\Core\Components\Web\CResponse'
        ],
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => '_kluatr_key_',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:dbname=stdp_first;host=jj.sel.pet4.ru:3306",
            //'emulatePrepare' => true,
            'username' => 'test',
            'password' => 'Es2LixutjQ_V-lGyZSBdaGP7B0aLhWJJ',
            'charset' => 'utf8',
            'commandClass' => 'Kluatr\Core\Components\Db\CCommand'
        ],

        'urlManager'=>[
            'class'=>'Kluatr\Core\Components\Web\Managers\CUrlManager',
            'enablePrettyUrl'=>true,
            'showScriptName'=>false,
            'enableStrictParsing'=>false,
            'rules'=>[
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ]
        ],

    ],

    'modules' => [],
    'params' => [],
];

return $config;
