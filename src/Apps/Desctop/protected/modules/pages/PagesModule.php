<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 11.12.2017
 * Time: 18:33
 */

namespace App\Modules\Pages;

use Kluatr\Core\Components\Web\AWebModule as BaseModule;

class PagesModule extends BaseModule
{
    public $controllerNamespace = "App\Modules\Pages\Controllers";

    public function init()
    {
        parent::init();
        //$this->_add_url_rule("/", "pages/page/main");
    }
}
