<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 22.01.18
 * Time: 15:10
 */

namespace App\Modules\Pages\Controllers;

use Kluatr\Core\Components\Web\AWebController as BaseController;
use Kluatr\Core\Components\Errors\BaseException;

/**
 * PageController маршруты для статических страниц.
 *
 * PageController выполняет маршрутизацию для отрисовки ститических страниц.
 *
 * @version 1.0
 * @author p.bunaev
 */

class PageController extends BaseController
{
    /**
     * Описывает роут для главной страницы
     * @return string
     */
    public function actionMain()
    {
        $error1 = new BaseException("error-1");
        $error2 = new BaseException("error-2", 500, $error1);
        #throw $error2;
        return "first page";
    }

    /**
     *  Дефолтный метод обрабатывающий исключения
     * @todo можно добавить сохранние исключений в бд
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionError()
    {
        //@todo if check is legasy url
        $this->_proxy_to_legasy();
        return;

        $exception = \Yii::$app->getErrorHandler()->exception;
        $info = $exception->getMessage()."\n".$exception->getFile();
        if (\method_exists($exception, "getErrorStringInfo") === true) {
            $info = $exception->getErrorStringInfo();
        }
        //var_dump($exception->getPrevious());
        return $info;
    }

    /**
     * Выполняет проксирование и запуск легаси проекта kluatr
     * @return void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function _proxy_to_legasy() :void
    {
        \Yii::$container->get('\Application')::run(\Application::MODE_WEB);
        return;
    }
}
