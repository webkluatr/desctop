<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 24.01.18
 * Time: 15:45
 */

namespace App\Modules\Legasy;

use Kluatr\Core\Components\Web\AWebModule as BaseModule;

class LegasyModule extends BaseModule
{
    public $controllerNamespace = "App\Modules\Legasy\Controllers";

    public function init()
    {
        parent::init();
    }
}
