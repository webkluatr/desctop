<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 24.01.18
 * Time: 15:46
 */

namespace App\Modules\Legasy\Controllers;

use Kluatr\Core\Components\Web\AWebController as BaseController;
use Kluatr\Core\Components\Errors\BaseException;

/**
 * Class ProxyController
 * Контроллер проксирует вызовы в legasy проект kluatr
 * @package App\Modules\Legasy\Controllers
 */
class ProxyController extends BaseController
{

    public function actionProxy()
    {
        return "proxy";
    }
}
