<?php
\defined('DS') or \define('DS', DIRECTORY_SEPARATOR);

#require_once $legasy_index;
#exit ("stop");
#exit;
$_vendors_path = \dirname(\dirname(\dirname(\dirname(\dirname(__FILE__))))).\DS."vendor".\DS;

// автолоад композера
$loader = require_once($_vendors_path."autoload.php");
// путь к приложению
$_app_path = \dirname(\dirname(__FILE__)).\DS."protected".\DS;

$config = require($_app_path."config".\DS.\App\Config\IConfigEnv::ENVIRONMENT.".config.php");
// register modules
$_modules = Kluatr\Core\Helpers\parseDirectory($_app_path . "modules" . \DS, "App\\Modules\\", true);

$modules = [];
// auto register modules
foreach ($_modules as $_class => $file) {
    $module = \basename($file, ".php");
    if (\strpos(\mb_strtolower($module), "module")!==false) {
        $module = \str_replace("module", "", \mb_strtolower($module));
        $modules[$module] = ["class"=>$_class];
    }
}

\Yii::$classMap = \array_merge(\Yii::$classMap, $_modules);
$config["modules"] = $modules;
#print_r($modules);
#exit;
//init app
$app = (new \yii\web\Application($config));

// remove not using variables
unset($_vendors_path, $_app_path, $config, $_modules, $modules);

//Помещаем легаси в контейнер
Yii::$container->set('\Application', \Application::class);

$app->run();
